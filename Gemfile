source 'https://rubygems.org'

gem 'volt'

# volt uses mongo as the default data store.
gem 'volt-mongo'

# The following gem's are optional for themeing
# Twitter bootstrap
gem 'volt-bootstrap'

# Simple theme for bootstrap, remove to theme yourself.
gem 'volt-bootstrap_jumbotron_theme'

# User templates for login, signup, and logout menu.
gem 'volt-user_templates'

# Add ability to send e-mail from apps.
gem 'volt-mailer'

# To stop Volt from being awful to me.
gem 'faye-websocket'
gem 'websocket-driver'

# To schedule map updates.
gem 'rufus-scheduler'

# Send some sms.
gem 'sms-easy'
gem 'actionmailer'
gem 'sendgrid-ruby'

# Use rbnacl for message bus encrpytion
# (optional, if you don't need encryption, disable in app.rb and remove)
#
# Message Bus encryption is not supported on Windows at the moment.
platform :ruby, :jruby do
  gem 'rbnacl', require: false
  gem 'rbnacl-libsodium', require: false
end

# Asset compilation gems, they will be required when needed.
gem 'csso-rails', require: false
gem 'uglifier', require: false

group :test do
  # Testing dependencies
  gem 'rspec'
  gem 'opal-rspec'
  gem 'capybara'
  gem 'selenium-webdriver'
  gem 'chromedriver2-helper'
  gem 'poltergeist'
end

# Server for MRI
platform :mri, :mingw do
  # The implementation of ReadWriteLock in Volt uses concurrent ruby and ext helps performance.
  gem 'concurrent-ruby-ext'

  # Thin is the default volt server, Puma is also supported
  gem 'thin'
  gem 'bson_ext'
end
